
docker build -t proxy-reconfigure-etcd-dev .
. /etc/environment

DOCKER_CONTAINER="default.rcore.uk"

docker kill ${DOCKER_CONTAINER}-discovery

docker rm ${DOCKER_CONTAINER}-discovery

docker run -ti --name ${DOCKER_CONTAINER}-discovery -e FLEET_ITER="1" -e DOCKER_CONTAINER=${DOCKER_CONTAINER} -e DOCKER_HOST=${COREOS_PRIVATE_IPV4} -e DOCKER_PORT="2375" -e ETCD_HOST=${COREOS_PRIVATE_IPV4} -e ETCD_PORT="4001" proxy-reconfigure-etcd-dev 

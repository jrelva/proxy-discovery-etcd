import os,time,json, etcd,sys
import requests

try:
	etcdPath = os.environ['ETCD_PATH']
except:
	etcdPath = '/apps'

try:
	etcdHost = os.environ['ETCD_HOST']
	etcdPort = os.environ['ETCD_PORT']
	dockerName = os.environ['DOCKER_CONTAINER']
	dockerHost = os.environ['DOCKER_HOST']
	dockerPort = os.environ['DOCKER_PORT']
	domain = os.environ['DOMAIN']
	fleetIteration = os.environ['FLEET_ITER']
except Exception as e:
	print 'You must specify all environment variables'
	print str(e)
	sys.exit(1)

etcdPath = etcdPath + '/' + domain

etcdclient = etcd.Client(host=etcdHost, port=int(etcdPort))
dockerUrl = 'http://' + dockerHost + ':' + dockerPort



def getSpecificContainer(jContainers):
	myContainer = None
	for container in jContainers:
		if '/'+dockerName in container['Names']:
			myContainer = container
	if not myContainer:
		print 'Unable to find ' + dockerName + ' in running containers'
		sys.exit(1)
	else:
		return myContainer

def getContainers():
	r = requests.get(url=dockerUrl+'/containers/json')
	if r.status_code != 200:
		print 'Unable to connect to Docker API'
		print 'Host: ' + dockerHost
		print 'Port: ' + dockerPort
		sys.exit(1)
	jContainers = json.loads(r.text)
	return jContainers

def extractNetwork(container):
	for ports in container['Ports']:
		if ports['PrivatePort'] == 80:
			return ports['IP'], ports['PublicPort']

def setEtcd(ip, port):
	etcdclient.write(etcdPath + '/host/vhost', domain, ttl=60)
	etcdclient.write(etcdPath + '/listener' + str(fleetIteration) + '/endpoint', ':'.join([ip,str(port)]), ttl=60)


def main():
	jContainers = getContainers()
	container = getSpecificContainer(jContainers)
	setEtcd(*extractNetwork(container))	




while True:
	main()
	time.sleep(45)


